/**********************************************************************************
 FSvsMAN.c    Compare freesurfer based ROIs with manual drawn ROI. Find the overlap
 volume of the two sets of ROIs. Print out a text file that contains the manual ROI
 volume, the freesurfer ROI name, the freesurfer ROI volume, the overlapping volume
 with and without a PETMASK.

 USAGE:
 FSvsMan FSfile MANROI PETMASK
 FSfile is the 4dfp file of the freesurfer segmentation output, typically 
 wmparc001.4dfp.img, MANROI is 4dfp file of the target manual drawn ROI to be 
 examined. PETMASK is a 4dfp file that specifies the PET field of view to make sure
 PET ROI analysis contains only voxels that are within PET FOV. The output file
 has the same name as the manual drawn ROI file, but with an extension of .txt. The
 input argument are specified without the file extensions (.4dfp.img).

  Yi Su, 03/08/2011
**********************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

int main(int argc, char **argv)
{
	int i, j, nv, NFS[87], NMAN, NFSPET[87], NMANPET, NBOTH[87], NBOTHPET[87];
	float *fs, *man, *fov;
	IMAGE_4dfp *FSdata=NULL, *MANdata=NULL, *PETMask=NULL;
	FILE *fp;
	char optf[MAXL];
	char *rnames[]={
		"Cerebellum-White-Matter",
		"Cerebellum-Cortex",
		"Thalamus-Proper",
		"Caudate",
		"Putamen",
		"Pallidum",
		"Hippocampus",
		"Amygdala",
		"Accumbens-area",
		"VentralDC",
		"Brain-Stem",
		"CC_Posterior",
		"CC_Mid_Posterior",
		"CC_Central",
		"CC_Mid_Anterior",
		"CC_Anterior",
		"ctx-bankssts",
		"ctx-caudalanteriorcingulate",
		"ctx-caudalmiddlefrontal",
		"ctx-corpuscallosum",
		"ctx-cuneus",
		"ctx-entorhinal",
		"ctx-fusiform",
		"ctx-inferiorparietal",
		"ctx-inferiortemporal",
		"ctx-isthmuscingulate",
		"ctx-lateraloccipital",
		"ctx-lateralorbitofrontal",
		"ctx-lingual",
		"ctx-medialorbitofrontal",
		"ctx-middletemporal",
		"ctx-parahippocampal",
		"ctx-paracentral",
		"ctx-parsopercularis",
		"ctx-parsorbitalis",
		"ctx-parstriangularis",
		"ctx-pericalcarine",
		"ctx-postcentral",
		"ctx-posteriorcingulate",
		"ctx-precentral",
		"ctx-precuneus",
		"ctx-rostralanteriorcingulate",
		"ctx-rostralmiddlefrontal",
		"ctx-superiorfrontal",
		"ctx-superiorparietal",
		"ctx-superiortemporal",
		"ctx-supramarginal",
		"ctx-frontalpole",
		"ctx-temporalpole",
		"ctx-transversetemporal",
		"ctx-insula",
		"wm-bankssts",
		"wm-caudalanteriorcingulate",
		"wm-caudalmiddlefrontal",
		"wm-corpuscallosum",
		"wm-cuneus",
		"wm-entorhinal",
		"wm-fusiform",
		"wm-inferiorparietal",
		"wm-inferiortemporal",
		"wm-isthmuscingulate",
		"wm-lateraloccipital",
		"wm-lateralorbitofrontal",
		"wm-lingual",
		"wm-medialorbitofrontal",
		"wm-middletemporal",
		"wm-parahippocampal",
		"wm-paracentral",
		"wm-parsopercularis",
		"wm-parsorbitalis",
		"wm-parstriangularis",
		"wm-pericalcarine",
		"wm-postcentral",
		"wm-posteriorcingulate",
		"wm-precentral",
		"wm-precuneus",
		"wm-rostralanteriorcingulate",
		"wm-rostralmiddlefrontal",
		"wm-superiorfrontal",
		"wm-superiorparietal",
		"wm-superiortemporal",
		"wm-supramarginal",
		"wm-frontalpole",
		"wm-temporalpole",
		"wm-transversetemporal",
		"wm-insula",
		"wm-unsegmented"
	};

	/* read FSdata, MANdata, PETMask */
	
	FSdata=read_image_4dfp(argv[1], FSdata);
	MANdata=read_image_4dfp(argv[2], MANdata);
	PETMask=read_image_4dfp(argv[3], PETMask);
	
	fs=FSdata->image;
	man=MANdata->image;
	fov=PETMask->image;

	nv = FSdata->ifh.matrix_size[0]*FSdata->ifh.matrix_size[1]*FSdata->ifh.matrix_size[2]*FSdata->ifh.matrix_size[3];
	NMAN=0;
	NMANPET=0;
	for (i=0; i<87; i++) {
		NFS[i]=0;
		NFSPET[i]=0; 
		NBOTH[i]=0;
		NBOTHPET[i]=0;	
	}
	for (i=0; i<nv; i++) {
		switch ((int) (*fs)) {
			case 7:
			case 46:
				NFS[0]=NFS[0]+1;
				if (*fov>0) {
					NFSPET[0]=NFSPET[0]+1;
					if (*man>0) {
						NBOTHPET[0]=NBOTHPET[0]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[0]=NBOTH[0]+1;
				}
				break;
				
			case 8:
			case 47:
				NFS[1]=NFS[1]+1;
				if (*fov>0) {
					NFSPET[1]=NFSPET[1]+1;
					if (*man>0) {
						NBOTHPET[1]=NBOTHPET[1]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[1]=NBOTH[1]+1;
				}
				break;
				
			case 10:
			case 49:
				NFS[2]=NFS[2]+1;
				if (*fov>0) {
					NFSPET[2]=NFSPET[2]+1;
					if (*man>0) {
						NBOTHPET[2]=NBOTHPET[2]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[2]=NBOTH[2]+1;
				}
				break;

			case 11:
			case 50:
				NFS[3]=NFS[3]+1;
				if (*fov>0) {
					NFSPET[3]=NFSPET[3]+1;
					if (*man>0) {
						NBOTHPET[3]=NBOTHPET[3]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[3]=NBOTH[3]+1;
				}
				break;
				
			case 12:
			case 51:
				NFS[4]=NFS[4]+1;
				if (*fov>0) {
					NFSPET[4]=NFSPET[4]+1;
					if (*man>0) {
						NBOTHPET[4]=NBOTHPET[4]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NBOTH[4]=NBOTH[4]+1;
					NMAN++;
				}
				break;
				
			case 13:
			case 52:
				NFS[5]=NFS[5]+1;
				if (*fov>0) {
					NFSPET[5]=NFSPET[5]+1;
					if (*man>0) {
						NBOTHPET[5]=NBOTHPET[5]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NBOTH[5]=NBOTH[5]+1;
					NMAN++;
				}
				break;

			case 17:
			case 53:
				NFS[6]=NFS[6]+1;
				if (*fov>0) {
					NFSPET[6]=NFSPET[6]+1;
					if (*man>0) {
						NBOTHPET[6]=NBOTHPET[6]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[6]=NBOTH[6]+1;
				}
				break;
				
			case 18:
			case 54:
				NFS[7]=NFS[7]+1;
				if (*fov>0) {
					NFSPET[7]=NFSPET[7]+1;
					if (*man>0) {
						NBOTHPET[7]=NBOTHPET[7]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[7]=NBOTH[7]+1;
				}
				break;
				
			case 26:
			case 58:
				NFS[8]=NFS[8]+1;
				if (*fov>0) {
					NFSPET[8]=NFSPET[8]+1;
					if (*man>0) {
						NBOTHPET[8]=NBOTHPET[8]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[8]=NBOTH[8]+1;
				}
				break;

			case 28:
			case 60:
				NFS[9]=NFS[9]+1;
				if (*fov>0) {
					NFSPET[9]=NFSPET[9]+1;
					if (*man>0) {
						NBOTHPET[9]=NBOTHPET[9]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[9]=NBOTH[9]+1;
				}
				break;
				
			case 16:
				NFS[10]=NFS[10]+1;
				if (*fov>0) {
					NFSPET[10]=NFSPET[10]+1;
					if (*man>0) {
						NBOTHPET[10]=NBOTHPET[10]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NBOTH[10]=NBOTH[10]+1;
					NMAN++;
				}
				break;
				
			case 251:
				NFS[11]=NFS[11]+1;
				if (*fov>0) {
					NFSPET[11]=NFSPET[11]+1;
					if (*man>0) {
						NBOTHPET[11]=NBOTHPET[11]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NBOTH[11]=NBOTH[11]+1;
					NMAN++;
				}
				break;
				
			case 252:
				NFS[12]=NFS[12]+1;
				if (*fov>0) {
					NFSPET[12]=NFSPET[12]+1;
					if (*man>0) {
						NBOTHPET[12]=NBOTHPET[12]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NBOTH[12]=NBOTH[12]+1;
					NMAN++;
				}
				break;
				
			case 253:
				NFS[13]=NFS[13]+1;
				if (*fov>0) {
					NFSPET[13]=NFSPET[13]+1;
					if (*man>0) {
						NBOTHPET[13]=NBOTHPET[13]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NBOTH[13]=NBOTH[13]+1;
					NMAN++;
				}
				break;
				
			case 254:
				NFS[14]=NFS[14]+1;
				if (*fov>0) {
					NFSPET[14]=NFSPET[14]+1;
					if (*man>0) {
						NBOTHPET[14]=NBOTHPET[14]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NBOTH[14]=NBOTH[14]+1;
					NMAN++;
				}
				break;
				
			case 255:
				NFS[15]=NFS[15]+1;
				if (*fov>0) {
					NFSPET[15]=NFSPET[15]+1;
					if (*man>0) {
						NBOTHPET[15]=NBOTHPET[15]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NBOTH[15]=NBOTH[15]+1;
					NMAN++;
				}
				break;

			case 1001:
			case 2001:
				NFS[16]=NFS[16]+1;
				if (*fov>0) {
					NFSPET[16]=NFSPET[16]+1;
					if (*man>0) {
						NBOTHPET[16]=NBOTHPET[16]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[16]=NBOTH[16]+1;
				}
				break;
				
			case 1002:
			case 2002:
				NFS[17]=NFS[17]+1;
				if (*fov>0) {
					NFSPET[17]=NFSPET[17]+1;
					if (*man>0) {
						NBOTHPET[17]=NBOTHPET[17]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[17]=NBOTH[17]+1;
				}
				break;
				
			case 1003:
			case 2003:
				NFS[18]=NFS[18]+1;
				if (*fov>0) {
					NFSPET[18]=NFSPET[18]+1;
					if (*man>0) {
						NBOTHPET[18]=NBOTHPET[18]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[18]=NBOTH[18]+1;
				}
				break;

			case 1004:
			case 2004:
				NFS[19]=NFS[19]+1;
				if (*fov>0) {
					NFSPET[19]=NFSPET[19]+1;
					if (*man>0) {
						NBOTHPET[19]=NBOTHPET[19]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[19]=NBOTH[19]+1;
				}
				break;

			case 1005:
			case 2005:
				NFS[20]=NFS[20]+1;
				if (*fov>0) {
					NFSPET[20]=NFSPET[20]+1;
					if (*man>0) {
						NBOTHPET[20]=NBOTHPET[20]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[20]=NBOTH[20]+1;
				}
				break;
				
			case 1006:
			case 2006:
				NFS[21]=NFS[21]+1;
				if (*fov>0) {
					NFSPET[21]=NFSPET[21]+1;
					if (*man>0) {
						NBOTHPET[21]=NBOTHPET[21]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[21]=NBOTH[21]+1;
				}
				break;
				
			case 1007:
			case 2007:
				NFS[22]=NFS[22]+1;
				if (*fov>0) {
					NFSPET[22]=NFSPET[22]+1;
					if (*man>0) {
						NBOTHPET[22]=NBOTHPET[22]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[22]=NBOTH[22]+1;
				}
				break;

			case 1008:
			case 2008:
				NFS[23]=NFS[23]+1;
				if (*fov>0) {
					NFSPET[23]=NFSPET[23]+1;
					if (*man>0) {
						NBOTHPET[23]=NBOTHPET[23]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[23]=NBOTH[23]+1;
				}
				break;
				
			case 1009:
			case 2009:
				NFS[24]=NFS[24]+1;
				if (*fov>0) {
					NFSPET[24]=NFSPET[24]+1;
					if (*man>0) {
						NBOTHPET[24]=NBOTHPET[24]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[24]=NBOTH[24]+1;
				}
				break;
				
			case 1010:
			case 2010:
				NFS[25]=NFS[25]+1;
				if (*fov>0) {
					NFSPET[25]=NFSPET[25]+1;
					if (*man>0) {
						NBOTHPET[25]=NBOTHPET[25]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[25]=NBOTH[25]+1;
				}
				break;

			case 1011:
			case 2011:
				NFS[26]=NFS[26]+1;
				if (*fov>0) {
					NFSPET[26]=NFSPET[26]+1;
					if (*man>0) {
						NBOTHPET[26]=NBOTHPET[26]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[26]=NBOTH[26]+1;
				}
				break;
				
			case 1012:
			case 2012:
				NFS[27]=NFS[27]+1;
				if (*fov>0) {
					NFSPET[27]=NFSPET[27]+1;
					if (*man>0) {
						NBOTHPET[27]=NBOTHPET[27]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[27]=NBOTH[27]+1;
				}
				break;

			case 1013:
			case 2013:
				NFS[28]=NFS[28]+1;
				if (*fov>0) {
					NFSPET[28]=NFSPET[28]+1;
					if (*man>0) {
						NBOTHPET[28]=NBOTHPET[28]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[28]=NBOTH[28]+1;
				}
				break;

			case 1014:
			case 2014:
				NFS[29]=NFS[29]+1;
				if (*fov>0) {
					NFSPET[29]=NFSPET[29]+1;
					if (*man>0) {
						NBOTHPET[29]=NBOTHPET[29]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[29]=NBOTH[29]+1;
				}
				break;

			case 1015:
			case 2015:
				NFS[30]=NFS[30]+1;
				if (*fov>0) {
					NFSPET[30]=NFSPET[30]+1;
					if (*man>0) {
						NBOTHPET[30]=NBOTHPET[30]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[30]=NBOTH[30]+1;
				}
				break;
				
			case 1016:
			case 2016:
				NFS[31]=NFS[31]+1;
				if (*fov>0) {
					NFSPET[31]=NFSPET[31]+1;
					if (*man>0) {
						NBOTHPET[31]=NBOTHPET[31]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[31]=NBOTH[31]+1;
				}
				break;
				
			case 1017:
			case 2017:
				NFS[32]=NFS[32]+1;
				if (*fov>0) {
					NFSPET[32]=NFSPET[32]+1;
					if (*man>0) {
						NBOTHPET[32]=NBOTHPET[32]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[32]=NBOTH[32]+1;
				}
				break;

			case 1018:
			case 2018:
				NFS[33]=NFS[33]+1;
				if (*fov>0) {
					NFSPET[33]=NFSPET[33]+1;
					if (*man>0) {
						NBOTHPET[33]=NBOTHPET[33]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[33]=NBOTH[33]+1;
				}
				break;
				
			case 1019:
			case 2019:
				NFS[34]=NFS[34]+1;
				if (*fov>0) {
					NFSPET[34]=NFSPET[34]+1;
					if (*man>0) {
						NBOTHPET[34]=NBOTHPET[34]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[34]=NBOTH[34]+1;
				}
				break;
				
			case 1020:
			case 2020:
				NFS[35]=NFS[35]+1;
				if (*fov>0) {
					NFSPET[35]=NFSPET[35]+1;
					if (*man>0) {
						NBOTHPET[35]=NBOTHPET[35]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[35]=NBOTH[35]+1;
				}
				break;

			case 1021:
			case 2021:
				NFS[36]=NFS[36]+1;
				if (*fov>0) {
					NFSPET[36]=NFSPET[36]+1;
					if (*man>0) {
						NBOTHPET[36]=NBOTHPET[36]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[36]=NBOTH[36]+1;
				}
				break;
				
			case 1022:
			case 2022:
				NFS[37]=NFS[37]+1;
				if (*fov>0) {
					NFSPET[37]=NFSPET[37]+1;
					if (*man>0) {
						NBOTHPET[37]=NBOTHPET[37]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[37]=NBOTH[37]+1;
				}
				break;

			case 1023:
			case 2023:
				NFS[38]=NFS[38]+1;
				if (*fov>0) {
					NFSPET[38]=NFSPET[38]+1;
					if (*man>0) {
						NBOTHPET[38]=NBOTHPET[38]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[38]=NBOTH[38]+1;
				}
				break;

			case 1024:
			case 2024:
				NFS[39]=NFS[39]+1;
				if (*fov>0) {
					NFSPET[39]=NFSPET[39]+1;
					if (*man>0) {
						NBOTHPET[39]=NBOTHPET[39]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[39]=NBOTH[39]+1;
				}
				break;

			case 1025:
			case 2025:
				NFS[40]=NFS[40]+1;
				if (*fov>0) {
					NFSPET[40]=NFSPET[40]+1;
					if (*man>0) {
						NBOTHPET[40]=NBOTHPET[40]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[40]=NBOTH[40]+1;
				}
				break;
				
			case 1026:
			case 2026:
				NFS[41]=NFS[41]+1;
				if (*fov>0) {
					NFSPET[41]=NFSPET[41]+1;
					if (*man>0) {
						NBOTHPET[41]=NBOTHPET[41]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[41]=NBOTH[41]+1;
				}
				break;
				
			case 1027:
			case 2027:
				NFS[42]=NFS[42]+1;
				if (*fov>0) {
					NFSPET[42]=NFSPET[42]+1;
					if (*man>0) {
						NBOTHPET[42]=NBOTHPET[42]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[42]=NBOTH[42]+1;
				}
				break;

			case 1028:
			case 2028:
				NFS[43]=NFS[43]+1;
				if (*fov>0) {
					NFSPET[43]=NFSPET[43]+1;
					if (*man>0) {
						NBOTHPET[43]=NBOTHPET[43]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[43]=NBOTH[43]+1;
				}
				break;
				
			case 1029:
			case 2029:
				NFS[44]=NFS[44]+1;
				if (*fov>0) {
					NFSPET[44]=NFSPET[44]+1;
					if (*man>0) {
						NBOTHPET[44]=NBOTHPET[44]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[44]=NBOTH[44]+1;
				}
				break;
				
			case 1030:
			case 2030:
				NFS[45]=NFS[45]+1;
				if (*fov>0) {
					NFSPET[45]=NFSPET[45]+1;
					if (*man>0) {
						NBOTHPET[45]=NBOTHPET[45]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[45]=NBOTH[45]+1;
				}
				break;

			case 1031:
			case 2031:
				NFS[46]=NFS[46]+1;
				if (*fov>0) {
					NFSPET[46]=NFSPET[46]+1;
					if (*man>0) {
						NBOTHPET[46]=NBOTHPET[46]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[46]=NBOTH[46]+1;
				}
				break;
				
			case 1032:
			case 2032:
				NFS[47]=NFS[47]+1;
				if (*fov>0) {
					NFSPET[47]=NFSPET[47]+1;
					if (*man>0) {
						NBOTHPET[47]=NBOTHPET[47]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[47]=NBOTH[47]+1;
				}
				break;

			case 1033:
			case 2033:
				NFS[48]=NFS[48]+1;
				if (*fov>0) {
					NFSPET[48]=NFSPET[48]+1;
					if (*man>0) {
						NBOTHPET[48]=NBOTHPET[48]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[48]=NBOTH[48]+1;
				}
				break;

			case 1034:
			case 2034:
				NFS[49]=NFS[49]+1;
				if (*fov>0) {
					NFSPET[49]=NFSPET[49]+1;
					if (*man>0) {
						NBOTHPET[49]=NBOTHPET[49]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[49]=NBOTH[49]+1;
				}
				break;

			case 1035:
			case 2035:
				NFS[50]=NFS[50]+1;
				if (*fov>0) {
					NFSPET[50]=NFSPET[50]+1;
					if (*man>0) {
						NBOTHPET[50]=NBOTHPET[50]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[50]=NBOTH[50]+1;
				}
				break;

			case 3001:
			case 4001:
				NFS[51]=NFS[51]+1;
				if (*fov>0) {
					NFSPET[51]=NFSPET[51]+1;
					if (*man>0) {
						NBOTHPET[51]=NBOTHPET[51]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[51]=NBOTH[51]+1;
				}
				break;
				
			case 3002:
			case 4002:
				NFS[52]=NFS[52]+1;
				if (*fov>0) {
					NFSPET[52]=NFSPET[52]+1;
					if (*man>0) {
						NBOTHPET[52]=NBOTHPET[52]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[52]=NBOTH[52]+1;
				}
				break;
				
			case 3003:
			case 4003:
				NFS[53]=NFS[53]+1;
				if (*fov>0) {
					NFSPET[53]=NFSPET[53]+1;
					if (*man>0) {
						NBOTHPET[53]=NBOTHPET[53]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[53]=NBOTH[53]+1;
				}
				break;

			case 3004:
			case 4004:
				NFS[54]=NFS[54]+1;
				if (*fov>0) {
					NFSPET[54]=NFSPET[54]+1;
					if (*man>0) {
						NBOTHPET[54]=NBOTHPET[54]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[54]=NBOTH[54]+1;
				}
				break;

			case 3005:
			case 4005:
				NFS[55]=NFS[55]+1;
				if (*fov>0) {
					NFSPET[55]=NFSPET[55]+1;
					if (*man>0) {
						NBOTHPET[55]=NBOTHPET[55]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[55]=NBOTH[55]+1;
				}
				break;
				
			case 3006:
			case 4006:
				NFS[56]=NFS[56]+1;
				if (*fov>0) {
					NFSPET[56]=NFSPET[56]+1;
					if (*man>0) {
						NBOTHPET[56]=NBOTHPET[56]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[56]=NBOTH[56]+1;
				}
				break;
				
			case 3007:
			case 4007:
				NFS[57]=NFS[57]+1;
				if (*fov>0) {
					NFSPET[57]=NFSPET[57]+1;
					if (*man>0) {
						NBOTHPET[57]=NBOTHPET[57]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[57]=NBOTH[57]+1;
				}
				break;

			case 3008:
			case 4008:
				NFS[58]=NFS[58]+1;
				if (*fov>0) {
					NFSPET[58]=NFSPET[58]+1;
					if (*man>0) {
						NBOTHPET[58]=NBOTHPET[58]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[58]=NBOTH[58]+1;
				}
				break;
				
			case 3009:
			case 4009:
				NFS[59]=NFS[59]+1;
				if (*fov>0) {
					NFSPET[59]=NFSPET[59]+1;
					if (*man>0) {
						NBOTHPET[59]=NBOTHPET[59]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[59]=NBOTH[59]+1;
				}
				break;
				
			case 3010:
			case 4010:
				NFS[60]=NFS[60]+1;
				if (*fov>0) {
					NFSPET[60]=NFSPET[60]+1;
					if (*man>0) {
						NBOTHPET[60]=NBOTHPET[60]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[60]=NBOTH[60]+1;
				}
				break;

			case 3011:
			case 4011:
				NFS[61]=NFS[61]+1;
				if (*fov>0) {
					NFSPET[61]=NFSPET[61]+1;
					if (*man>0) {
						NBOTHPET[61]=NBOTHPET[61]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[61]=NBOTH[61]+1;
				}
				break;
				
			case 3012:
			case 4012:
				NFS[62]=NFS[62]+1;
				if (*fov>0) {
					NFSPET[62]=NFSPET[62]+1;
					if (*man>0) {
						NBOTHPET[62]=NBOTHPET[62]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[62]=NBOTH[62]+1;
				}
				break;

			case 3013:
			case 4013:
				NFS[63]=NFS[63]+1;
				if (*fov>0) {
					NFSPET[63]=NFSPET[63]+1;
					if (*man>0) {
						NBOTHPET[63]=NBOTHPET[63]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[63]=NBOTH[63]+1;
				}
				break;

			case 3014:
			case 4014:
				NFS[64]=NFS[64]+1;
				if (*fov>0) {
					NFSPET[64]=NFSPET[64]+1;
					if (*man>0) {
						NBOTHPET[64]=NBOTHPET[64]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[64]=NBOTH[64]+1;
				}
				break;

			case 3015:
			case 4015:
				NFS[65]=NFS[65]+1;
				if (*fov>0) {
					NFSPET[65]=NFSPET[65]+1;
					if (*man>0) {
						NBOTHPET[65]=NBOTHPET[65]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[65]=NBOTH[65]+1;
				}
				break;
				
			case 3016:
			case 4016:
				NFS[66]=NFS[66]+1;
				if (*fov>0) {
					NFSPET[66]=NFSPET[66]+1;
					if (*man>0) {
						NBOTHPET[66]=NBOTHPET[66]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[66]=NBOTH[66]+1;
				}
				break;
				
			case 3017:
			case 4017:
				NFS[67]=NFS[67]+1;
				if (*fov>0) {
					NFSPET[67]=NFSPET[67]+1;
					if (*man>0) {
						NBOTHPET[67]=NBOTHPET[67]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[67]=NBOTH[67]+1;
				}
				break;

			case 3018:
			case 4018:
				NFS[68]=NFS[68]+1;
				if (*fov>0) {
					NFSPET[68]=NFSPET[68]+1;
					if (*man>0) {
						NBOTHPET[68]=NBOTHPET[68]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[68]=NBOTH[68]+1;
				}
				break;
				
			case 3019:
			case 4019:
				NFS[69]=NFS[69]+1;
				if (*fov>0) {
					NFSPET[69]=NFSPET[69]+1;
					if (*man>0) {
						NBOTHPET[69]=NBOTHPET[69]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[69]=NBOTH[69]+1;
				}
				break;
				
			case 3020:
			case 4020:
				NFS[70]=NFS[70]+1;
				if (*fov>0) {
					NFSPET[70]=NFSPET[70]+1;
					if (*man>0) {
						NBOTHPET[70]=NBOTHPET[70]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[70]=NBOTH[70]+1;
				}
				break;

			case 3021:
			case 4021:
				NFS[71]=NFS[71]+1;
				if (*fov>0) {
					NFSPET[71]=NFSPET[71]+1;
					if (*man>0) {
						NBOTHPET[71]=NBOTHPET[71]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[71]=NBOTH[71]+1;
				}
				break;
				
			case 3022:
			case 4022:
				NFS[72]=NFS[72]+1;
				if (*fov>0) {
					NFSPET[72]=NFSPET[72]+1;
					if (*man>0) {
						NBOTHPET[72]=NBOTHPET[72]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[72]=NBOTH[72]+1;
				}
				break;

			case 3023:
			case 4023:
				NFS[73]=NFS[73]+1;
				if (*fov>0) {
					NFSPET[73]=NFSPET[73]+1;
					if (*man>0) {
						NBOTHPET[73]=NBOTHPET[73]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[73]=NBOTH[73]+1;
				}
				break;

			case 3024:
			case 4024:
				NFS[74]=NFS[74]+1;
				if (*fov>0) {
					NFSPET[74]=NFSPET[74]+1;
					if (*man>0) {
						NBOTHPET[74]=NBOTHPET[74]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[74]=NBOTH[74]+1;
				}
				break;

			case 3025:
			case 4025:
				NFS[75]=NFS[75]+1;
				if (*fov>0) {
					NFSPET[75]=NFSPET[75]+1;
					if (*man>0) {
						NBOTHPET[75]=NBOTHPET[75]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[75]=NBOTH[75]+1;
				}
				break;
				
			case 3026:
			case 4026:
				NFS[76]=NFS[76]+1;
				if (*fov>0) {
					NFSPET[76]=NFSPET[76]+1;
					if (*man>0) {
						NBOTHPET[76]=NBOTHPET[76]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[76]=NBOTH[76]+1;
				}
				break;
				
			case 3027:
			case 4027:
				NFS[77]=NFS[77]+1;
				if (*fov>0) {
					NFSPET[77]=NFSPET[77]+1;
					if (*man>0) {
						NBOTHPET[77]=NBOTHPET[77]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[77]=NBOTH[77]+1;
				}
				break;

			case 3028:
			case 4028:
				NFS[78]=NFS[78]+1;
				if (*fov>0) {
					NFSPET[78]=NFSPET[78]+1;
					if (*man>0) {
						NBOTHPET[78]=NBOTHPET[78]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[78]=NBOTH[78]+1;
				}
				break;
				
			case 3029:
			case 4029:
				NFS[79]=NFS[79]+1;
				if (*fov>0) {
					NFSPET[79]=NFSPET[79]+1;
					if (*man>0) {
						NBOTHPET[79]=NBOTHPET[79]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[79]=NBOTH[79]+1;
				}
				break;
				
			case 3030:
			case 4030:
				NFS[80]=NFS[80]+1;
				if (*fov>0) {
					NFSPET[80]=NFSPET[80]+1;
					if (*man>0) {
						NBOTHPET[80]=NBOTHPET[80]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[80]=NBOTH[80]+1;
				}
				break;

			case 3031:
			case 4031:
				NFS[81]=NFS[81]+1;
				if (*fov>0) {
					NFSPET[81]=NFSPET[81]+1;
					if (*man>0) {
						NBOTHPET[81]=NBOTHPET[81]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[81]=NBOTH[81]+1;
				}
				break;
				
			case 3032:
			case 4032:
				NFS[82]=NFS[82]+1;
				if (*fov>0) {
					NFSPET[82]=NFSPET[82]+1;
					if (*man>0) {
						NBOTHPET[82]=NBOTHPET[82]+1;
						NMANPET++;
					}					
				}
				if (*man>0) {
					NMAN++;
					NBOTH[82]=NBOTH[82]+1;
				}
				break;

			case 3033:
			case 4033:
				NFS[83]=NFS[83]+1;
				if (*fov>0) {
					NFSPET[83]=NFSPET[83]+1;
					if (*man>0) {
						NBOTHPET[83]=NBOTHPET[83]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[83]=NBOTH[83]+1;
				}
				break;

			case 3034:
			case 4034:
				NFS[84]=NFS[84]+1;
				if (*fov>0) {
					NFSPET[84]=NFSPET[84]+1;
					if (*man>0) {
						NBOTHPET[84]=NBOTHPET[84]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[84]=NBOTH[84]+1;
				}
				break;

			case 3035:
			case 4035:
				NFS[85]=NFS[85]+1;
				if (*fov>0) {
					NFSPET[85]=NFSPET[85]+1;
					if (*man>0) {
						NBOTHPET[85]=NBOTHPET[85]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[85]=NBOTH[85]+1;
				}
				break;
				
			case 5001:
			case 5002:
				NFS[86]=NFS[86]+1;
				if (*fov>0) {
					NFSPET[86]=NFSPET[86]+1;
					if (*man>0) {
						NBOTHPET[86]=NBOTHPET[86]+1;
						NMANPET++;
					}
				}
				if (*man>0) {
					NMAN++;
					NBOTH[86]=NBOTH[86]+1;
				}
				break;
				

			default :
				if (*man>0) {
					NMAN++;
					if (*fov>0) {
						NMANPET++;
					}
				}
			
		}
		fs++;
		fov++;
		man++;
	}
	/* Write output file */
	sprintf(optf, "%s.txt", argv[2]);
	if (!(fp=fopen(optf,"w"))) errw("FSvsMAN",optf);
	fprintf(fp, "%-35s%16s%16s%16s%16s%16s%16s\n", "Structure_Name", "NMAN", "NMANPET", "NFS", "NFSPET", "OVERLAP", "OVERLAPPET");
	for (i=0;i<87;i++) {
		fprintf(fp, "%-35s%16d%16d%16d%16d%16d%16d\n", rnames[i], NMAN, NMANPET, NFS[i], NFSPET[i], NBOTH[i], NBOTHPET[i]);
	}
	
}
