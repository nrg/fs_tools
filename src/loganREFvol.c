#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

bool file_exists(const char * filename)
{
	FILE *file;
	if(file = fopen(filename, "r")) {fclose(file);        return true;   }  return false;
}
void readPIBtac(char *fn, float *tac, float *rsftac, float *frd, float *st, float *nv, int *nframes)
{
	FILE *fp;
	char line[MAXL], dummy[MAXL];
	float fdum, *fptr1, *fptr2, *fptr3, *fptr4;
	int i;
	
	if (!(fp=fopen(fn, "r"))) errr("ROIPIB","ROIs");
	fgets(line, 512, fp);
	sscanf(line, "%s%s%s%s%s%s%f", dummy, dummy, dummy, dummy, dummy, dummy, nv);
	fptr3=tac;
	fptr4=rsftac;
	fptr2=frd;
	fptr1=st;
	*nframes=0;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%f%f%f%f%f", &fdum, fptr1, fptr2, fptr3, fptr4);
		fptr1++;
		fptr2++;
		fptr3++;
		fptr4++;
		(*nframes)++;
	}
	fclose(fp);
}

int main(int argc, char **argv)
{
	char dpetf[MAXL], maskf[MAXL], reftacf[MAXL], optroot[MAXL];
	FILE *fp;
	int sf, ef, i;
	float tac[MAXL], tmp[MAXL], frd[MAXL], st[MAXL], nv, nframes;
	
	
}
