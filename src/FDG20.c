/*
This Program scan through multi-frame PET images and determines the frame 
range of 2 parts ( c1 and c2 ).

USAGE:
	FDG20 PETfile model_duration
	PETfile is a 4dfp image file
	model_duration is the duration of for the subsequent modeling 
	step
	modeling refers to SUVR calculation.

Yi Su, 2011-10-25	

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

void readinfo(char *fn, float *st, float *t, float *frd, float *df, int *frame, int *nf)
{
	FILE *fp;
	char line[MAXL], dummy[MAXL];
	float fdum, *fptr1, *fptr2, *fptr3, *fptr4;
	int i, *iptr5;
	
	if (!(fp=fopen(fn, "r"))) errr("readinfo",fn);
	fptr1=st;
	fptr2=t;
	fptr3=frd;
	fptr4=df;
	iptr5=frame;
	*nf=0;
	while(fgets(line, 512, fp))
	{
		sscanf(line, "%f%f%f%f%f",fptr1, fptr2, fptr3, fptr4, iptr5);
		fptr1++;
		fptr2++;
		fptr3++;
		fptr4++;
		iptr5++;
		(*nf)++;
	}
	fclose(fp);
}

int main(int argc, char **argv)
{
float st[MAXL], t[MAXL], frd[MAXL], df[MAXL], ttoe[MAXL], big, mindiff, diff, mdt, dt2, ts;
int frame[MAXL], nf, i, j, startframe[2], endframe[2], vflag;
char fn[MAXL];

if (argc<3) {
	printf("USAGE: FDG20 PETfile(4dfp) model_duration(in minutes)\n");
	exit(-1);
}
strcpy(fn, argv[1]);
mdt=atof(argv[2]);
mdt=mdt*60;
dt2=mdt/2.;

readinfo(fn, st, t, frd, df, frame, &nf);
if (nf<2) exit(-1);

big=3600;

endframe[1]=nf;


/* Determine startframe[0] (c1) */
ttoe[nf-1]=frd[nf-1];
for (i=nf-2; i>0; i--)
{
	ttoe[i]=ttoe[i+1]+frd[i];
}
mindiff=big;
for(i=0; i<nf; i++)
{
	diff=fabs(ttoe[i]-mdt);
	if (diff<mindiff)
	{	
		mindiff=diff;
		startframe[0]=i+1;
	}
}

/* Determine startframe[1] (c2) */
i=startframe[0]+1;
ts=dt2;
mindiff=big;
while (i<nf)
{
	diff=fabs(ttoe[i]-dt2);
	if (diff<mindiff)
	{
		mindiff=diff;
		startframe[1]=i+1;
	}
	i++;
}
endframe[0]=startframe[1]-1;


/* Validity Check */
vflag=0;
for (i=0; i<2; i++)
{
	if (startframe[i]>nf || startframe[i]<1) vflag=1;
	if (endframe[i]>nf || endframe[i]<1) vflag=1;
}
if (vflag>0) return(-1);

printf("startframe=( %d %d  )\n", startframe[0], startframe[1]);
printf("lastframe=( %d %d  )\n", endframe[0], endframe[1]);
return(0);
}
