#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

int main(int argc, char **argv)
{
	int i, j, nr, nv, col;
	char **RNAME=NULL;
	char line1[512], roimeanfile[256], mask4dfp[256], outroot[256];
	float val1[256], val2[256], val3[256], val4[256], tmp, *val;
	float *fptr1, *fptr2;
	FILE *fp1;
	IMAGE_4dfp *Mask=NULL, *outimg=NULL;
	
	strcpy(roimeanfile, argv[1]);
	strcpy(mask4dfp, argv[2]);
	strcpy(outroot, argv[3]);
	col=atoi(argv[4]);
	
	RNAME=(char **)malloc((size_t)(256*sizeof(char *)));
	if (!RNAME) nrerror("malloc error");
	for (i=0;i<256;i++) {
		RNAME[i]=(char *)malloc((size_t)(256*sizeof(char)));
		if (!RNAME[i]) nrerror("malloc error");
	}
	
	if (! (fp1=fopen(roimeanfile, "r"))) nrerror("file open error");

	fgets(line1, 512, fp1);
	nr=0;
	while (fgets(line1, 512, fp1))
	{
		sscanf(line1, "%s%f%f%f%f", RNAME[nr], val1+nr, val2+nr, val3+nr, val4+nr);
		nr++;
	}
	fclose(fp1);
	
	Mask=read_image_4dfp(mask4dfp, Mask);
	outimg=(IMAGE_4dfp *)malloc((size_t)sizeof(IMAGE_4dfp));
	if (!outimg) errm("roilstto4dfp");
	nv=Mask->ifh.matrix_size[0]*Mask->ifh.matrix_size[1]*Mask->ifh.matrix_size[2]*Mask->ifh.matrix_size[3];
	outimg->image=(float *)malloc((size_t)(nv*sizeof(float)));
	if (!outimg->image) errm("roilstto4dfp");
	strcpy(outimg->ifh.interfile, Mask->ifh.interfile);
	strcpy(outimg->ifh.version_of_keys, Mask->ifh.version_of_keys);
	strcpy(outimg->ifh.conversion_program, Mask->ifh.conversion_program);
	strcpy(outimg->ifh.name_of_data_file, outroot);
	strcpy(outimg->ifh.number_format, Mask->ifh.number_format);
	strcpy(outimg->ifh.imagedata_byte_order, Mask->ifh.imagedata_byte_order);
	outimg->ifh.number_of_bytes_per_pixel=Mask->ifh.number_of_bytes_per_pixel;
	outimg->ifh.number_of_dimensions=Mask->ifh.number_of_dimensions;
	for (i=0; i<3; i++)
	{
		outimg->ifh.matrix_size[i]=Mask->ifh.matrix_size[i];
		outimg->ifh.scaling_factor[i]=Mask->ifh.scaling_factor[i];
		outimg->ifh.mmppix[i]=Mask->ifh.mmppix[i];
		outimg->ifh.center[i]=Mask->ifh.center[i];
	}
	outimg->ifh.matrix_size[3]=Mask->ifh.matrix_size[3];
	outimg->ifh.scaling_factor[3]=Mask->ifh.scaling_factor[3];
	outimg->ifh.orientation=Mask->ifh.orientation;
	
	switch (col) {
		case 1: val=val1;
		break;
		case 2: val=val2;
		break;
		case 3: val=val3;
		break;
		case 4: val=val4;
		break;
		default:
		nrerror("invalid column");
	}
	
	
	fptr1=Mask->image;
	fptr2=outimg->image;
	for (i=0;i<nv;i++)
	{
		tmp=rint((double)*fptr1);
		*fptr2=val[(int)tmp];
		fptr1++;
		fptr2++;
	}
	write_image_4dfp(outroot, outimg);
}
