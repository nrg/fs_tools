#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

#ifndef SMALL
#define SMALL 1.0e-2
#endif



IMAGE_4dfp *patlakvol(IMAGE_4dfp *petdata, IMAGE_4dfp *mask, float *t, float *aif, float *frd, int nframe, int stframe, IMAGE_4dfp *patlakout)
{
	float *petptr, *maskptr, *yptr, *tac, *aif_int, *patlakx, *a, *outptr;
	float k, mx, my, *xt, yt, sxx, sxy, syy, Sxx, Sx, Sy, Syy, Sxy, r, intercept, slope, delta;
	int nvoxs, ndix, i, j, isbig;
	IMAGE_4dfp *patlaky=NULL;
	
	/* Validate Inputs */
	if (!petdata) {printf("Invalid PET data.\n"); exit(1);}
	if (!mask) {printf("Invalid Mask.\n"); exit(1);}
	if (petdata->ifh.matrix_size[3]!=nframe) {
		printf("Total frames (%d) of the PET data does not match nframe (%d).\n", petdata->ifh.matrix_size[4], nframe);
		exit (1);
	}
	if ( petdata->ifh.matrix_size[0]!=mask->ifh.matrix_size[0]
		||petdata->ifh.matrix_size[1]!=mask->ifh.matrix_size[1]
		||petdata->ifh.matrix_size[2]!=mask->ifh.matrix_size[2]) {
		printf ("petdata size does not match mask volume.\n");
		exit (1);
	}
	if (stframe>(nframe-1)) {printf("Bad starting frame.\n"); exit(1);}

	
	/* Initialize output data volume */
	nvoxs = petdata->ifh.matrix_size[0]*petdata->ifh.matrix_size[1]*petdata->ifh.matrix_size[2];
	ndix = nvoxs * petdata->ifh.matrix_size[3];
	patlakout=(IMAGE_4dfp *)malloc((size_t)sizeof(IMAGE_4dfp));
	if (!patlakout) errm("patlakvol");
	isbig = strcmp(petdata->ifh.imagedata_byte_order, "littleendian");
	patlakout->image=(float *)malloc((size_t)(nvoxs*4*sizeof(float)));
	if (!patlakout->image) errm("patlakvol");
	strcpy(patlakout->ifh.interfile, petdata->ifh.interfile);
	strcpy(patlakout->ifh.version_of_keys, petdata->ifh.version_of_keys);
	strcpy(patlakout->ifh.conversion_program, petdata->ifh.conversion_program);
	strcpy(patlakout->ifh.name_of_data_file, "patlakout");
	strcpy(patlakout->ifh.number_format, petdata->ifh.number_format);
	strcpy(patlakout->ifh.imagedata_byte_order, petdata->ifh.imagedata_byte_order);
	patlakout->ifh.number_of_bytes_per_pixel=petdata->ifh.number_of_bytes_per_pixel;
	patlakout->ifh.number_of_dimensions=petdata->ifh.number_of_dimensions;
	for (i=0; i<3; i++)
	{
		patlakout->ifh.matrix_size[i]=petdata->ifh.matrix_size[i];
		patlakout->ifh.scaling_factor[i]=petdata->ifh.scaling_factor[i];
		patlakout->ifh.mmppix[i]=petdata->ifh.mmppix[i];
		patlakout->ifh.center[i]=petdata->ifh.center[i];
	}
	patlakout->ifh.matrix_size[3]=4;
	patlakout->ifh.scaling_factor[3]=petdata->ifh.scaling_factor[3];
	patlakout->ifh.orientation=petdata->ifh.orientation;

	/* Integrate AIF and calculate x, y axes for patlak analysis */
	
	patlaky=copy_image_4dfp(petdata, patlaky);
	if (!patlaky) {printf("Image Copy Failure: patlaky.\n"); exit(1);}
	
	aif_int=vector(0,nframe-1);
	a=vector(0,nframe-1);
	patlakx=vector(0,nframe-1);
	a[0]=aif[0]*frd[0]/2.;
	aif_int[0]=a[0];
	patlakx[0]=aif_int[0]/(aif[0]+SMALL);
	for (i=1; i<nframe; i++)
	{
		a[i]=aif[i]*frd[i]/2.0;
		aif_int[i]=aif_int[i-1]+a[i-1]+a[i];
		patlakx[i]=aif_int[i]/(aif[i]+SMALL);		
	}
	
	petptr = petdata->image;
	yptr = patlaky->image;
		
	for (i=0; i<nframe; i++)
	{
		for (j=0; j<nvoxs; j++)
		{
			*yptr=*petptr/(aif[i]+SMALL);
			yptr++; petptr++;
		}
	}
	
	
	/* Perform patlak analysis */
	
	petptr = petdata->image;
	maskptr = mask->image;
	yptr = patlaky->image;
	outptr = patlakout->image;
	mx=0.;
	k=(float)(nframe-stframe+1);
	Sxx=0.;
	for (i=stframe-1; i<nframe; i++)
	{
		mx += patlakx[i];
		Sxx += patlakx[i]*patlakx[i];
	}
	Sx = mx;
	mx /= k;
	sxx = Sxx - k*mx*mx;
	xt=vector(0, nframe-1);
	delta = k*Sxx - Sx*Sx;
	
	for (i=stframe-1; i<nframe; i++)
	{
		xt[i] = patlakx[i]-mx;
	}
	
	
	for (i=0; i<nvoxs; i++)
	{
		if (*maskptr>0.){
		my=0.;
		Syy=0.;
		Sxy=0.;
		for (j=stframe-1; j<nframe; j++)
		{
			yt = *(yptr + j*nvoxs + i);
			my += yt;
			Syy += yt*yt;
			Sxy += patlakx[j]*yt;	
		}
		
		Sy = my;
		my /= k;
		syy = Syy - k*my*my;
		sxy=0.;
		for (j=stframe-1; j<nframe; j++)
		{
			yt = *(yptr + j*nvoxs + i) - my;
			sxy += xt[j]*yt;
		}
		if (sxx*syy>0) {
			r=sxy/sqrtf(sxx*syy);
		} else {
			r=0;
		}
		intercept=(Sxx*Sy-Sx*Sxy)/delta;
		slope=(k*Sxy-Sx*Sy)/delta;
		
		*(outptr + i)=slope;
		*(outptr + nvoxs + i)=intercept;
		*(outptr + 2*nvoxs + i)=slope/intercept;
		*(outptr + 3*nvoxs + i)=r*r;
		/*printf("%f\n",slope);*/
		}
		maskptr++;
	}
	
	free_vector(xt, 0, nframe-1);
	free_vector(aif_int, 0, nframe-1);
	free_vector(a, 0, nframe-1);
	free_vector(patlakx, 0, nframe-1);
	return (patlakout);
}
