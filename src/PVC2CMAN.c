/**********************************************************************************
 Two compartment partial volume correction for brain PET image analysis, using
 manual drawn ROIs, based on a two component model, i.e. brain tissue and
 others (including background, csf, which are assumed to have no signal).
  
 USAGE:
 PVC2CMAN FSfile PETMASK fwhm
 FSfile is the 4dfp file of the freesurfer segmentation. It serves as the basis
 for creating brain tissue mask.
 PETMASK is a 4dfp file that specifies the PET field of view to make sure PET ROI 
 analysis contains only voxels that are within PET FOV. The manual drawn ROIs are
 assumed to have the file name .
 The output file has the name PVC2CMAN.txt, that has three columns. The first 
 column is the ROI label and the second column is the recovery coefficient, which 
 should be used as the denominator to correct for partial volume effects based on 
 two component mode. The third column is the roi size within PET FOV. A second
 output is a 4dfp file that is the Gaussian blurred version of the brain tissue mask.    

 Yi Su, 03/21/2011
**********************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <endianio.h>
#include <Getifh.h>
#include <rec.h>
#include <librms.h>
#include <nrutil.h>
#include <RSF.h>

int main(int argc, char **argv)
{
	int i, j, nvroi[10], m2, nroi, nx, ny, nz, tmp;
	float *fptr1, *fptr2, *fptr3, cmppix[3], fhalf, pvc[10];
	IMAGE_4dfp *ROIMask=NULL, *PETMask=NULL, *FSdata=NULL, *mask=NULL;
	FILE *fp;
	char optf[MAXL], tmpstr[MAXL], line[512];
	char *regions[] = {
		"cerebellumROI",
		"brainstemROI",
		"occipitalROI",
		"prefrontalROI",
		"lattempROI",	
		"precuneusROI",
		"caudateROI",
		"gyrusrectusROI",
		"parietalROI",
		"rostralROI"
	};

	/* read FSdata, PETMask */
	FSdata=read_image_4dfp(argv[1], FSdata);
	PETMask=read_image_4dfp(argv[2], PETMask);
	
	
	/* allocate memory for mask */
	m2=FSdata->ifh.number_of_bytes_per_pixel*FSdata->ifh.matrix_size[0]*FSdata->ifh.matrix_size[1]*FSdata->ifh.matrix_size[2]*FSdata->ifh.matrix_size[3];
	mask=(IMAGE_4dfp *)malloc((size_t)sizeof(IMAGE_4dfp));
	if (!mask) errm("PVC2CMAN");
	mask->image=(float *)malloc((size_t)m2);
	if (!mask->image) errm("PVC2CMAN");	
	m2=m2/FSdata->ifh.number_of_bytes_per_pixel;
	strcpy(mask->ifh.interfile, FSdata->ifh.interfile);
	strcpy(mask->ifh.version_of_keys, FSdata->ifh.version_of_keys);
	strcpy(mask->ifh.conversion_program, FSdata->ifh.conversion_program);
	
	strcpy(mask->ifh.number_format, FSdata->ifh.number_format);
	strcpy(mask->ifh.imagedata_byte_order, FSdata->ifh.imagedata_byte_order);
	mask->ifh.number_of_bytes_per_pixel=FSdata->ifh.number_of_bytes_per_pixel;
	mask->ifh.number_of_dimensions=FSdata->ifh.number_of_dimensions;
	for (i=0; i<3; i++)
	{
		mask->ifh.matrix_size[i]=FSdata->ifh.matrix_size[i];
		mask->ifh.scaling_factor[i]=FSdata->ifh.scaling_factor[i];
		mask->ifh.mmppix[i]=FSdata->ifh.mmppix[i];
		mask->ifh.center[i]=FSdata->ifh.center[i];
	}
	mask->ifh.matrix_size[3]=FSdata->ifh.matrix_size[3];
	mask->ifh.scaling_factor[3]=FSdata->ifh.scaling_factor[3];
	mask->ifh.orientation=FSdata->ifh.orientation;
	
	/* create binary mask that defines brain tissue against CSF and background */
	fptr1=FSdata->image;
	fptr2=mask->image;
	tmp=0;
	for (i=0; i<m2; i++)
	{
		if ( (int)(*fptr1)==0 || (int)(*fptr1)==4  || (int)(*fptr1)==5  || 
		    (int)(*fptr1)==14 || (int)(*fptr1)==15 || (int)(*fptr1)==30 ||
		    (int)(*fptr1)==43 || (int)(*fptr1)==44 || (int)(*fptr1)==62 ||
		    (int)(*fptr1)==72 || (int)(*fptr1)==73 || (int)(*fptr1)==74 ||
		    (int)(*fptr1)==75 || (int)(*fptr1)==76 || (int)(*fptr1)==24 ||
		    (int)(*fptr1)==98 || ((int)(*fptr1)>=118 && (int)(*fptr1)<=158) ||
		    (int)(*fptr1)==165 || (int)(*fptr1)==166 || (int)(*fptr1)==167 ||
		    (int)(*fptr1)==168 || ((int)(*fptr1)>=169 && (int)(*fptr1)<=215) ||
		    (int)(*fptr1)==217 || (int)(*fptr1)==221 || (int)(*fptr1)== 224 ||
		    ((int)(*fptr1)>=256 && (int)(*fptr1)<=499) || 
		    ((int)(*fptr1)>=559 && (int)(*fptr1)<=701) ||
		    ((int)(*fptr1)>=704 && (int)(*fptr1)<=999)) {
		    *fptr2=0;
		    }
		else {
		    *fptr2=1;
		    tmp++;
		    }
		fptr1++;
		fptr2++;
	}
	printf("%d\n",tmp);
	printf("Done...Binarize FS mask\n");

	/* convolve binary brain mask with gaussian kernel */
	fhalf = 4.412712/atof(argv[3]); /* argv[3] is the fwhm of the gaussian kernal in mm */
	nx = FSdata->ifh.matrix_size[0];
	ny = FSdata->ifh.matrix_size[1];
	nz = FSdata->ifh.matrix_size[2];
	cmppix[0]=FSdata->ifh.scaling_factor[0]/10.;
	cmppix[1]=FSdata->ifh.scaling_factor[1]/10.;
	cmppix[2]=FSdata->ifh.scaling_factor[2]/10.;
	sprintf(mask->ifh.name_of_data_file, "BrainTissueMask");
	write_image_4dfp(mask->ifh.name_of_data_file, mask);
	
	gauss3d(mask->image, &nx, &ny, &nz, cmppix, &fhalf);
	sprintf(mask->ifh.name_of_data_file, "BrainTissueMask_g%d",(int)(fhalf*10));
	write_image_4dfp(mask->ifh.name_of_data_file, mask);
	printf("Done...Gaussian blur brain mask\n");
	
	/* calculate regional correction factor */	
	for (i=0; i<10; i++){
		pvc[i]=0;
		nvroi[i]=0;
		sprintf(tmpstr, "%s.4dfp.img", regions[i]);
		fp=fopen(tmpstr,"r");
		if (fp) {
			ROIMask=read_image_4dfp(regions[i], ROIMask);
			fptr1=mask->image;
			fptr2=ROIMask->image;
			fptr3=PETMask->image;
			for (j=0; j<m2; j++){
				if (*fptr2>0 && *fptr3>0) {
					pvc[i]+=*fptr1;
					nvroi[i]++;
				}
				fptr1++;
				fptr2++;
				fptr3++;
			}
			pvc[i]=pvc[i]/(float)nvroi[i];			
		fclose(fp);
		}
		else {
			printf("ROI file does not exist: %s\n", regions[i]);
		}
	}
	
	printf("Done...calculating correction factor\n");
	
	/* print output */
	if (!(fp = fopen("PVC2CMAN.txt","w"))) errw("PVC2CMAN", "PVC2CMAN.txt");
	for (i=0; i<10; i++) {
		if (nvroi>0){
			fprintf(fp, "%-35s%10.4f%10d\n", regions[i], pvc[i], nvroi[i]);
		} else {
			fprintf(fp, "%-35s%10s%10d\n", regions[i], "NA", 0);
		}
	}
	
	printf("Done...print output\n");
	
	fclose(fp);
	free(mask->image);
	free(mask);
	
	return 0;
}
