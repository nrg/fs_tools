      subroutine hesstest(quad,n,p_curr,p_rang)
      real*4 quad(n,n)	! symmetric matrix defines toy objective function
      real*4 p_curr(n)	! curent parameters (input)
      real*4 p_rang(n)	! nominal parameter search range (input)
      real*4 p_temp(n)	! temp parameters copy
      real*4 p_radi(n)	! adjusted parameter search radius
      real*4 p_best(n)	! best parameters
      real*4 p_grad(n)	! objective function gradient
      real*4 hess(n,n)	! hessian matrix
      real*4 hest(n,n)	! temp matrix copy
      real*4 eigv(n,n)	! matrix eigenvectors
      logical*4 lrestart/.false./
      logical*4 ldebug/.true./
      pointer (pp_grad,p_grad),(pp_temp,p_temp),(pp_radi,p_radi),(pp_best,p_best),(phess,hess),(peigv,eigv),(phest,hest)

      pp_temp=malloc(4*n)	! FORTRAN malloc
      pp_grad=malloc(4*n)
      pp_radi=malloc(4*n)
      pp_best=malloc(4*n)
      phess=malloc(4*n*n)
      peigv=malloc(4*n*n)
      phest=malloc(4*n*n)
      if(pp_temp.eq.0.or.pp_grad.eq.0.or.pp_radi.eq.0.or.pp_best.eq.0.or.peigv.eq.0.or.phess.eq.0.or.phest.eq.0)
     &stop 'hesstest memory allocation error'

      call matcop(quad,hest,n)
      call eigen(hest,eigv,n)
      write(*,"('quad eigenvalues')")
      write(*,102)(hest(i,i),i=1,n)

      escale=1.
      do j=1,n
        p_radi(j)=p_rang(j)
        p_best(j)=p_curr(j)
      enddo
      call eval_quad(quad,n,p_curr,eta)
      write(*,"('parameters')")
      write(*,102)(p_curr(j),j=1,n)
      write(*,"('eta',f10.6)")eta

      rscale=1.
      nnn=0
      niter=10
   79 eta0=eta
   78 if(lrestart)then
        do j=1,n
          p_curr(j)=p_best(j)
        enddo
        lrestart=.false.
        write(*,"('restart rscale ',f7.2,' ->',f7.2)")rscale,rscale*1.5
        rscale=rscale*1.5
      endif
      write(*,"('parameter search radius')")
      write(*,102)(rscale*p_radi(j),j=1,n)
      write(*,"('niter,escale ',i5,f10.2)")niter,escale

      write(*,"('evaluating gradient and hessian diagonals')")
      do 71 j=1,n
      do k=1,n
        p_temp(k)=p_curr(k)
      enddo
      g=0.
      h=-2.*eta0
      do 72 i=-1,1,2
      p_temp(j)=p_curr(j)+float(i)*p_radi(j)*rscale
      call eval_quad(quad,n,p_temp,eta)
      g=g+float(i)*eta
   72 h=h+eta
      p_grad(j)=0.5*g*escale
   71 hess(j,j)=h*escale

      write(*,"('evaluating hessian off-diagonals')")
      do 73 j=1,n-1
      do 73 jj=j+1,n
      do k=1,n
        p_temp(k)=p_curr(k)
      enddo
      h=0.
      do 75 i =-1,1,2
      do 75 ii=-1,1,2
      p_temp(j) =p_curr(j) +float(i) *p_radi(j) *rscale
      p_temp(jj)=p_curr(jj)+float(ii)*p_radi(jj)*rscale
      call eval_quad(quad,n,p_temp,eta)
   75 h=h+float(ii*i)*eta
      hess(j,jj)=0.25*h*escale
   73 hess(jj,j)=hess(j,jj)

      if(ldebug)then
        write(*, "('linear system')")
        do i=1,n
          write(*,"(f8.4,2x,12f8.4)")p_grad(i),(hess(i,j),j=1,n)
        enddo
      endif

      call matcop(hess,hest,n)
      call eigen(hest,eigv,n)
      write(*,"('hess eigenvalues')")
      write(*,102)(hest(i,i),i=1,n)
      cndnum=hest(1,1)/hest(n,n)
      if(cndnum.lt.1.)cndnum=1./cndnum

      call matcop(hess,hest,n)
      call matinv(hess,n,det)
      write(*,"('hess determinant ',e10.4,'  condition number ',e10.4)")det,cndnum
      if(cndnum.lt.0.)then
        nnn=nnn+1
	if(nnn.gt.2)then
          write(*,"('failed convergence')")
          return
        endif
        lrestart=.true.
        goto 78
      endif

      do 77 j=1,n
      do 77 i=1,n
   77 p_curr(j)=p_curr(j)-hess(j,i)*p_grad(i)*p_radi(j)*rscale
      write(*,"('parameters')")
      write(*,102)(p_curr(j),j=1,n)
      call eval_quad(quad,n,p_curr,eta)
      write(*,"('eta',f10.6)")eta
      if(eta.lt.0.95*eta0.and.cndnum>50.)then
        lrestart=.true.
        write(*,"('lrestart=',l1)")lrestart
        goto 78
      endif
      if(eta.gt.eta0)then
        delmax=0.
        do j=1,n
          t=abs(p_best(j)-p_curr(j))
          if(t.gt.delmax)delmax=t
          p_best(j)=p_curr(j)
        enddo
        write(*,"('maximum parameter change',f10.6)")delmax
        eta0=eta
      else
        do j=1,n
          p_curr(j)=p_best(j)
        enddo
        eta=eta0
      endif

      write(*,"('adjusting parameter search radius')")
      squeeze=alog10(float(n)*15./cndnum)
      squeeze=amax1(squeeze,0.5)
      squeeze=amin1(squeeze,2.0)
      do j=1,n
        t=abs(hest(j,j))
c       write(*,"('t=',f10.6)")t
        p_radi(j)=p_radi(j)/sqrt(squeeze*t)
      enddo
      escale=escale*squeeze
      niter=niter-1
      if(niter.gt.0)goto 79      

      call eval_quad(quad,n,p_curr,eta)
      write(*,"('parameters')")
      write(*,102)(p_curr(j),j=1,n)
      write(*,"('eta',f10.6)")eta

      call free(pp_temp)	! FORTRAN free
      call free(pp_grad)
      call free(pp_radi)
      call free(pp_best)
      call free(phess)
      call free(peigv)
      call free(phest)
      return
  102 format(6f10.4)
      end

      subroutine eval_quad(quad,n,p_curr,eta)
      real*4 quad(n,n),p_curr(n)
      t=0.
      do 1 i=1,n
      do 1 j=1,n
    1 t=t+quad(i,j)*p_curr(i)*p_curr(j)
      eta=exp(-t)
c     write(*,"('eta=',f10.6)")eta
      return
      end
