/********************************************************************************************/
/* Copyright 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006 		            */	
/* Washington University, Mallinckrodt Institute of Radiology.                              */
/* All Rights Reserved.                                                                     */
/* This software may not be reproduced, copied, or distributed without written              */
/* permission of Washington University. For further information contact A. Z. Snyder.       */
/********************************************************************************************/
/*$Header: /data/petsun4/data1/src_solaris/algebra_4dfp/RCS/get_4d_images2.c,v 1.6 2006/03/16 05:45:00 avi Exp $*/
/*$Log: get_4d_images2.c,v $
 * Revision 1.6  2006/03/16  05:45:00  avi
 * new local #include ifh.h
 *
 * Revision 1.5  2004/09/22  00:58:28  avi
 * get_4dfp_dimo_quiet ()
 *
 * Revision 1.4  2004/09/21  23:50:18  rsachs
 * Fixed a logic error.
 *
 * Revision 1.3  2004/03/11  05:41:42  avi
 * correct srgv in get_4dfp_dimo ()
 *
 * Revision 1.2  2004/03/11  05:24:28  avi
 * correct orient value limit check
 *
 * Revision 1.1  2004/03/11  05:02:52  avi
 * Initial revision
 *
 * Revision 1.3  2000/12/19  05:47:29  avi
 * correct fileroot parsing
 *
 * Revision 1.2  2000/12/19  00:16:51  avi
 * eliminate blind ERRR: code
 *
 * Revision 1.1  2000/12/13  07:26:28  avi
 * Initial revision
 **/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <ifh.h>		/* ifh */

#define MAXL		256

static void errr (char* program, char* filespc) {
	fprintf (stderr, "%s: %s read error\n", program, filespc);
	exit (-1);
}

static void getroot (char *filespc, char *imgroot) {
	char	*str;
	strcpy (imgroot, filespc);
	while (str = strrchr (imgroot, '.')) {
			if (!strcmp (str, ".rec"))	*str = '\0';
		else	if (!strcmp (str, ".img"))	*str = '\0';
		else	if (!strcmp (str, ".ifh"))	*str = '\0';
		else	if (!strcmp (str, ".4dfp"))	*str = '\0';
		else	if (!strcmp (str, ".hdr"))	*str = '\0';
		else	break;
	}
}

static char rcsid[] = "$Id: get_4d_images2.c,v 1.6 2006/03/16 05:45:00 avi Exp $";
int get_4dfp_dimo (char *fileroot, int *imgdim, float *voxsiz, int *orient) {
	FILE		*fp;
	IFH		ifh;
	static char	srgv[] = "get_4dfp_dimo";
	char		filespc[MAXL];
	int		i, k, status;
	char		*TCS[3] = {"T", "C", "S"};

	getroot (fileroot, filespc);
	if ((i = strlen (filespc)) + 10 > MAXL) {
		fprintf (stdout, "%s: %s filename too long\n", srgv, fileroot);
		return -1;
	}

	status = 0;
	sprintf (filespc, "%s.4dfp.img", filespc);
	if (Getifh (filespc, &ifh)) errr (srgv, filespc);
	for (k = 0; k < 3; k++) voxsiz[k] = ifh.scaling_factor[k];
	for (k = 0; k < ifh.number_of_dimensions; k++) imgdim[k] = ifh.matrix_size[k];

	fprintf (stdout, "%s\n", fileroot);
	fprintf (stdout, "%10d%10d%10d%10d\n", imgdim[0], imgdim[1], imgdim[2], imgdim[3]);
	fprintf (stdout, "%10f%10f%10f\n", voxsiz[0], voxsiz[1], voxsiz[2]);
	*orient = ifh.orientation;
	if (*orient < 2 || *orient > 4) {
		fprintf (stderr, "%s warning: %s illegal orientation (%d)\n", srgv, fileroot, ifh.orientation);
		status = -2;
	} else {
		fprintf (stdout, "orientation %s\n", TCS[*orient - 2]);
	}
	return status;
}

int get_4dfp_dimo_quiet (char *fileroot, int *imgdim, float *voxsiz, int *orient) {
	FILE		*fp;
	IFH		ifh;
	static char	srgv[] = "get_4dfp_dimo_quiet";
	char		filespc[MAXL];
	int		i, k, status;
	char		*TCS[3] = {"T", "C", "S"};

	getroot (fileroot, filespc);
	if ((i = strlen (filespc)) + 10 > MAXL) {
		fprintf (stderr, "%s: %s filename too long\n", srgv, fileroot);
		return -1;
	}

	status = 0;
	sprintf (filespc, "%s.4dfp.img", filespc);
	if (Getifh (filespc, &ifh)) errr (srgv, filespc);
	for (k = 0; k < 3; k++) voxsiz[k] = ifh.scaling_factor[k];
	for (k = 0; k < ifh.number_of_dimensions; k++) imgdim[k] = ifh.matrix_size[k];

	*orient = ifh.orientation;
	if (*orient < 2 || *orient > 4) {
		fprintf (stderr, "%s warning: %s illegal orientation (%d)\n", srgv, fileroot, ifh.orientation);
		status = -2;
	}
	return status;
}

int get_4d_dimensions (char *fileroot, int *imgdim, float *voxsiz) {
	FILE		*fp;
	IFH		ifh;
	static char	srgv[] = "get_4d_dimensions";
	char		filespc[MAXL];
	int		i, k, status = 0;

	getroot (fileroot, filespc);
	if ((i = strlen (filespc)) + 10 > MAXL) {
		fprintf (stderr, "%s: %s filename too long\n", srgv, fileroot);
		return i;
	}

	sprintf (filespc, "%s.4dfp.img", fileroot);
	if (Getifh (filespc, &ifh)) errr (srgv, filespc);
	for (k = 0; k < 3; k++) voxsiz[k] = ifh.scaling_factor[k];
	for (k = 0; k < ifh.number_of_dimensions; k++) imgdim[k] = ifh.matrix_size[k];

	fprintf (stdout, "%s\n", fileroot);
	fprintf (stdout, "%s dimensions\n", filespc);
	fprintf (stdout, "%10d%10d%10d%10d\n", imgdim[0], imgdim[1], imgdim[2], imgdim[3]);
	fprintf (stdout, "%10f%10f%10f\n", voxsiz[0], voxsiz[1], voxsiz[2]);
	return status;
}

void load_4d_frame (char *fileroot, int *imgdim, int frame, float *fimg) {
	FILE		*fp;
	static char	srgv[] = "load_4d_frame";
	char		filespc[MAXL];
	float		*fptr;
	int		vdim, i, k;

	vdim = imgdim[0] * imgdim[1] * imgdim[2];

	sprintf (filespc, "%s.4dfp.img", fileroot);
	fprintf (stdout, "Reading: %s frame %d\n", filespc, frame + 1);
	if (!(fp = fopen (filespc, "rb")) || fseek (fp, (long) frame * vdim * sizeof (float), SEEK_SET)
	|| fread (fimg, sizeof (float), vdim, fp) != vdim || fclose (fp)) errr (srgv, filespc);
}











