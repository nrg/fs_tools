#$Header: /data/petsun4/data1/src_solaris/TRX/RCS/test_rec.mak,v 1.3 2011/04/08 23:54:56 avi Exp $
#$Log: test_rec.mak,v $
# Revision 1.3  2011/04/08  23:54:56  avi
# make all sources CSRCS
#
# Revision 1.2  2006/09/23  06:31:38  avi
# LOBJS = endianio.o Getifh.o
#
# Revision 1.1  2006/03/24  06:47:35  avi
# Initial revision
#

PROG	= test_rec
CSRCS	= ${PROG}.c rec.c endianio.c Getifh.c
FSRCS	= 
OBJS	= ${CSRCS:.c=.o} ${FSRCS:.f=.o}
LOBJS	=
LIBS	= -lm

.c.o:
	${CC} -c $<

.f.o:
	${FC} -c $<

CC	= cc -O -I.
FC	= f77 -O -e -I4

${PROG}: ${OBJS}
	${CC} -o $@ ${OBJS} ${LOBJS} ${LIBS}

clean:
	/bin/rm ${PROG} ${OBJS}

release: ${PROG}
	chmod 755 ${PROG}
	chgrp program ${PROG}
	/bin/mv ${PROG} ${RELEASE}
