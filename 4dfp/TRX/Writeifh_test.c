/*$Header: /data/petsun4/data1/src_solaris/TRX/RCS/Writeifh_test.c,v 1.2 2007/02/28 07:00:10 avi Exp $*/
/*$Log: Writeifh_test.c,v $
 * Revision 1.2  2007/02/28  07:00:10  avi
 * Solaris 10
 *
 * Revision 1.1  2006/03/26  23:06:59  avi
 * Initial revision
 **/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Getifh.h>
#include <endianio.h>

#define MAXL	256

static char rcsid[] = "$Id: Writeifh_test.c,v 1.2 2007/02/28 07:00:10 avi Exp $";
int main (int argc, char *argv[]) {
	IFH		ifh;
	char		filespc[MAXL], imgroot[MAXL], outroot[MAXL];
	char		control = '\0';

/***********/
/* utility */
/***********/
	char		*str, command[MAXL], program[MAXL];
	int 		c, i, j, k;

/*********/
/* flags */
/*********/
	int		verbose = 0;
	int		debug = 0;
	int		isbig;

	printf ("%s\n", rcsid);
	if (!(str = strrchr (argv[0], '/'))) str = argv[0]; else str++;
	strcpy (program, str);

/************************/
/* process command line */
/************************/
	for (k = 0, i = 1; i < argc; i++) {
		if (*argv[i] == '-') {
			strcpy (command, argv[i]); str = command;
			while (c = *str++) switch (c) {
				case '@': control = *str++;	*str = '\0'; break;
				case 'v': verbose++;		break;
			}
		} else switch (k) {
			case 0:	getroot (argv[i], imgroot);	k++; break;
		}	
	}
	if (k < 1) {
		printf ("Usage:\t%s <(4dfp) file>\n", program);
		printf ("\toption\n");
		printf ("\t-@<b|l>\tspecify imagedata byte order (default CPU endian)\n");
		printf ("N.B.:	output ifh filename root = \"<input>\"_test\n");
		exit (1);
	}

	sprintf (filespc, "%s.4dfp.ifh", imgroot);
	if (Getifh (filespc, &ifh)) errr (program, filespc);

	sprintf (outroot, "%s_test", imgroot);
	sprintf (filespc, "%s.4dfp.ifh", outroot);
	if (Writeifh (program, outroot, &ifh, control)) errw (program, filespc);

	exit (0);
}
