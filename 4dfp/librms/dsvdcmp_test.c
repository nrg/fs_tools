/*$Header$*/
/*$Log$*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <float.h>
#include <string.h>

extern double	dnormal (void);
/**************/
/* dsvdcmp0.c */
/**************/
extern void	 dsvdcmp0  (double **a, int m, int n, double *w, double **v);
extern int	ndsvdcmp0  (double **a, int m, int n, double *w, double **v, double tol);
extern int	mdsvdcmp0  (double **a, int m, int n, double *w, double **v, double tol);

static char	program[256] = "dsvdcmp_test";

void errm (char* program) {
	fprintf (stderr, "%s: memory allocation error\n", program);
	exit (-1);
}

double **calloc_double2 (int n1, int n2) {
	int	i;
	double	**a;

	if (!(a = (double **) malloc (n1 * sizeof (double *)))) errm (program);
	if (!(a[0] = (double *) calloc (n1 * n2, sizeof (double)))) errm (program);
	for (i = 1; i < n1; i++) a[i] = a[0] + i*n2;
	return a;
}

void free_double2 (double **a) {
	free (a[0]);
	free (a);
}

void dmatlst (double **a, int ncol, int npts) {
        int     i, j;

        for (i = 0; i < npts; i++) {
                for (j = 0; j < ncol; j++) printf (" %11.7f", a[j][i]);
                printf ("\n");
        }
}

int main0 (int argc, char *argv[]) {
	int NPTS = 15, NCOL = 8;
	double	**E, **F, **G, **V, *W, errmax, q, tol = 1.e-7;
	int	i, j, k, n, iter = 0;
	time_t	time_sec;
	char	string[256];

	E = calloc_double2 (NCOL, NPTS);
	F = calloc_double2 (NCOL, NPTS);
	G = calloc_double2 (NCOL, NPTS);
	V = calloc_double2 (NPTS, NPTS);
	if (!(W = (double *) malloc (NPTS*sizeof (double)))) errm (program);

	if (1) {
		for (j = 0; j <  NCOL; j++) for (i = 0; i < NPTS; i++) E[j][i] = dnormal ();
	} else {
		E[0][0] = 2.;	E[1][0] =  0.;
		E[0][1] = 0.;	E[1][1] = -3.;
	}
if (0)	for (j = 4; j < NCOL; j++) {
		for (i = 0; i < NPTS; i++) E[j][i] = E[0][i];
	}
if (1) {
	printf ("E before dsvdcmp0()\n");
	dmatlst  (E, NCOL, NPTS); printf ("\n");
}
	dsvdcmp0 (E, NCOL, NPTS, W, V);
if (1) {
	printf ("E after dsvdcmp0()\n");
	dmatlst (E, NCOL, NPTS); printf ("\n");
	printf ("V after dsvdcmp0()\n");
	dmatlst (V, NPTS, NPTS); printf ("\n");
	printf ("W after dsvdcmp0()\n");
	for (i = 0; i < NPTS; i++) printf (" %11.7f", W[i]); printf ("\n"); printf ("\n");
}

if (1) {
	n = ndsvdcmp0 (E, NCOL, NPTS, W, V, tol);
	printf ("reduced dimensionality = %d\n", n);
	printf ("W after ndsvdcmp0()\n");
	for (i = 0; i < n; i++) printf (" %11.7f", W[i]); printf ("\n"); printf ("\n");
	printf ("E after ndsvdcmp0()\n");
	dmatlst (E, NCOL, n); printf ("\n");
	printf ("V after ndsvdcmp0()\n");
	dmatlst (V, NPTS, n); printf ("\n");
}

	free (W); free_double2 (E); free_double2 (F); free_double2 (V); free_double2 (G);
	exit (0);
}

int main_colcomb (int argc, char *argv[]) {
	int NPTS = 5, NCOL = 8;
	double	**E, **F, **G, **I, **V, **C, *W, errmax, q, tol = 1.e-5;
	int	i, j, k, n, iter = 0;
	time_t	time_sec;
	char	string[256];

while (iter++ < 1) {
	NCOL = 8;
	printf ("iter = %d\tncol = %d\n", iter, NCOL);
	time (&time_sec);
	strcpy (string, ctime (&time_sec));
	string[24] = '\0';
	printf ("%s\n", string);

	E = calloc_double2 (NCOL, NPTS);
	F = calloc_double2 (NCOL, NPTS);
	C = calloc_double2 (NCOL, NPTS);
	G = calloc_double2 (NCOL, NPTS);
	V = calloc_double2 (NPTS, NPTS);
	if (!(W = (double *) malloc (NPTS*sizeof (double)))) errm (program);

	if (1) {
		for (j = 0; j <  NCOL; j++) for (i = 0; i < NPTS; i++) E[j][i] = dnormal ();
	} else {
		E[0][0] = 2.;	E[1][0] =  0.;
		E[0][1] = 0.;	E[1][1] = -3.;
	}
if (0)	for (j = 4; j < NCOL; j++) {
		for (i = 0; i < NPTS; i++) E[j][i] = E[0][i];
	}
if (0) {
	printf ("E before dsvdcmp0()\n");
	dmatlst  (E, NCOL, NPTS); printf ("\n");
}
	dsvdcmp0 (E, NCOL, NPTS, W, V);
if (0) {
	printf ("E after dsvdcmp0()\n");
	dmatlst (E, NCOL, NPTS); printf ("\n");
	printf ("V after dsvdcmp0()\n");
	dmatlst (V, NPTS, NPTS); printf ("\n");
	printf ("W after dsvdcmp0()\n");
	for (i = 0; i < NPTS; i++) printf (" %11.7f", W[i]); printf ("\n"); printf ("\n");
}

if (0) {
	n = ndsvdcmp0 (E, NCOL, NPTS, W, V, tol);
	printf ("reduced NPTS = %d\n", n);
	printf ("W after ndsvdcmp0()\n");
	for (i = 0; i < n; i++) printf (" %11.7f", W[i]); printf ("\n"); printf ("\n");
	printf ("E after ndsvdcmp0()\n");
	dmatlst (E, NCOL, n); printf ("\n");
	printf ("V after ndsvdcmp0()\n");
	dmatlst (V, NPTS, n); printf ("\n");
}

/*****************************/
/* show that V is orthogonal */
/*****************************/
	I = calloc_double2 (NPTS, NPTS);
	for (j = 0; j < NPTS; j++) for (i = 0; i < NPTS; i++) {
		for (k = 0; k < NPTS; k++) I[j][i] += V[i][k]*V[j][k];
	}
	printf ("I\n");
	dmatlst (I, NPTS, NPTS); printf ("\n");
	free_double2 (I);

/********************************************************/
/* create in G a linear combination of the columns of E */
/********************************************************/
	for (j = NCOL - 2; j >= 0; j--) for (i = 0; i < NPTS; i++) {
		G[j][i] += E[j + 1][i];
	}
	printf ("G\n");
	dmatlst (G, NCOL, NPTS); printf ("\n");

/**************************/
/* compute G coefficinets */
/**************************/
	for (j = 0; j < NCOL; j++) for (i = 0; i < NPTS; i++) {
		for (k = 0; k < NPTS; k++) C[j][i] += V[i][k]*G[j][k];
	}
	printf ("C\n");
	dmatlst (C, NCOL, NPTS); printf ("\n");

/***********************/
/* reconstitute G in F */
/***********************/
	for (j = 0; j < NCOL; j++) for (i = 0; i < NPTS; i++) {
		for (k = 0; k < NPTS; k++) F[j][i] += V[k][i]*C[j][k];
	}
	printf ("F\n");
	dmatlst (F, NCOL, NPTS); printf ("\n");

	errmax = 0.;
	for (j = 0; j < NCOL; j++) for (i = 0; i < NPTS; i++) {
		G[j][i] -= F[j][i];
		if (fabs (G[j][i]) > errmax) errmax = fabs (G[j][i]);
	}
if (0) {
	printf ("error\n");
	dmatlst (G, NCOL, NPTS); printf ("\n");
}
	printf ("errmax=%12.4e\n", errmax);
	if (errmax > tol) break;

	free (W); free_double2 (E); free_double2 (F); free_double2 (V); free_double2 (G);
	free_double2 (C);

	fflush (stdout);
}	/* iter loop */

	exit (0);
}

int main (int argc, char *argv[]) {
	int NPTS = 80, NCOL = 50;
	double	**E, **F, **G, **V, *W, errmax, q, tol = 1.e-6;
	int	i, j, k, n, iter = 0;
	time_t	time_sec;
	char	string[256];

while (iter++ < 10) {
/*	NCOL = 10*iter;	*/
	printf ("iter = %d\tncol = %d\n", iter, NCOL);
	time (&time_sec);
	strcpy (string, ctime (&time_sec));
	string[24] = '\0';
	printf ("%s\n", string);

	E = calloc_double2 (NCOL, NPTS);
	F = calloc_double2 (NCOL, NPTS);
	G = calloc_double2 (NCOL, NPTS);
	V = calloc_double2 (NPTS, NPTS);
	if (!(W = (double *) malloc (NPTS*sizeof (double)))) errm (program);

	if (1) {
		for (j = 0; j <  NCOL; j++) for (i = 0; i < NPTS; i++) E[j][i] = dnormal ();
	} else {
		E[0][0] = 2.;	E[1][0] =  0.;
		E[0][1] = 0.;	E[1][1] = -3.;
	}
if (0)	for (j = 4; j < NCOL; j++) {
		for (i = 0; i < NPTS; i++) E[j][i] = E[0][i];
	}
	for (j = 0; j < NCOL; j++) for (i = 0; i < NPTS; i++) F[j][i] = E[j][i];
if (0) {
	printf ("E before dsvdcmp0()\n");
	dmatlst  (E, NCOL, NPTS); printf ("\n");
}
	dsvdcmp0 (E, NCOL, NPTS, W, V);
if (0) {
	printf ("E after dsvdcmp0()\n");
	dmatlst (E, NCOL, NPTS); printf ("\n");
	printf ("V after dsvdcmp0()\n");
	dmatlst (V, NPTS, NPTS); printf ("\n");
}
	printf ("W after dsvdcmp0()\n");
	for (i = 0; i < NPTS; i++) printf (" %11.7f", W[i]); printf ("\n"); printf ("\n");

	if (1) {
		n = ndsvdcmp0 (E, NCOL, NPTS, W, V, tol);
	} else {
		n = mdsvdcmp0 (E, NCOL, NPTS, W, V, tol);
	}
	printf ("reduced dimensionality = %d\n", n);
	printf ("W after ndsvdcmp0()\n");
	for (i = 0; i < n; i++) printf (" %11.7f", W[i]); printf ("\n"); printf ("\n");
if (0) {
	printf ("E after ndsvdcmp0()\n");
	dmatlst (E, NCOL, n); printf ("\n");
	printf ("V after ndsvdcmp0()\n");
	dmatlst (V, NPTS, n); printf ("\n");
}

	for (j = 0; j < NCOL; j++) for (i = 0; i < n; i++) E[j][i] *= W[i];

if (0) {
	printf ("W*E\n");
	dmatlst (E, NCOL, n); printf ("\n");
}
	for (j = 0; j < NCOL; j++) for (i = 0; i < NPTS; i++) {
		for (k = 0; k < n; k++) G[j][i] += V[i][k]*E[j][k];
	}

if (0) {
	printf ("Vt*W*E\n");
	dmatlst (G, NCOL, NPTS); printf ("\n");
}
	errmax = 0.;
	for (j = 0; j < NCOL; j++) for (i = 0; i < NPTS; i++) {
		G[j][i] -= F[j][i];
		if (fabs (G[j][i]) > errmax) errmax = fabs (G[j][i]);
	}
if (0) {
	printf ("error\n");
	dmatlst (G, NCOL, NPTS); printf ("\n");
}
	printf ("errmax=%12.4e\n", errmax);
/*	if (errmax > tol) break;	*/

	free (W); free_double2 (E); free_double2 (F); free_double2 (V); free_double2 (G);
	fflush (stdout);
}	/* iter loop */

	exit (0);
}
